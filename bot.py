import discord
import asyncio
import random

client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
    if message.content.lower().startswith("/r "):
        roll = message.content.lower()[3:]
        tab = roll.split("d")
        if len(tab) == 2:
            nb_dice = int(tab[0])
            if nb_dice >= 1 and nb_dice < 600:
                limit_dice = int(tab[1])
                if limit_dice > 0:
                    dice = []
                    somme = 0
                    for i in range(0, nb_dice):
                        dice.append(int(random.random() * limit_dice) + 1)
                        somme += dice[i]
                        i += 1
                    if nb_dice == 1:
                        await client.send_message(message.channel, message.author.mention + " a fait un " + str(dice).strip('[]'))
                    else:
                        try:
                            await client.send_message(message.channel, message.author.mention + " a fait {" + str(dice).strip('[]') + "} pour un total de " + str(somme))
                        except discord.errors.HTTPException:
                            await client.send_message(message.channel, message.author.mention + " tu es un gros fils de pute ^^")
                else:
                    await client.send_message(message.channel, message.author.mention + " a l'air de pensé que les dès 0 existent...")
            else:
                await client.send_message(message.channel, "Merci de rentrer un nombre de dès compris en 1 et 600 ^^")


client.run('token')